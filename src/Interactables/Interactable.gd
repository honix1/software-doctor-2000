# Inherit from this node and add it to any object/character that you can interact with
class_name Interactable
extends Area

# Types of interactions:
#    Character Dialog
#    World Popup (simple descriptions about an object or place)
#    Start Minigame

# Main should connect to this signal
# The msg should be an array with different outputs based on the InteractionType
# See main.gd for what should be in each message
signal interacted_with(interaction_type, msg)

var is_player_near := false setget set_is_player_near

enum InteractionTypes { DIALOG, WORLD_OBJECT, MINIGAME }
var my_interaction_type: int
var msg := []

onready var icon: Sprite3D = $InteractIcon


func _ready() -> void:
	icon.hide()


# Player can call this function when his Interact raycast collides with this area
func interact_with() -> void:
	if my_interaction_type == null:
		return
	
	if not is_player_near:
		return
	
	emit_signal("interacted_with", my_interaction_type, msg)


func set_is_player_near(value: bool) -> void:
	is_player_near = value
	
	icon.visible = is_player_near


func get_interactable() -> Interactable:
	return self


func set_inactive() -> void:
	$CollisionShape.call_deferred("set_disabled", true)


func set_active() -> void:
	$CollisionShape.call_deferred("set_disabled", false)
