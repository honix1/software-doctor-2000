class_name InteractableWorldObject
extends Interactable

export var object_name: String
export var text_to_display: String


func _ready() -> void:
	._ready()
	my_interaction_type = InteractionTypes.WORLD_OBJECT
	if owner:
		yield(owner, "ready")
	msg = [object_name, text_to_display]


func interact_with() -> void:
	# Put anything exclusive to world_object interactables here
	
	.interact_with()
