class_name InteractableDialog
extends Interactable

var character_name: String
var char_state: String
var my_npc


func _ready() -> void:
	._ready()
	my_interaction_type = InteractionTypes.DIALOG
	if owner:
		yield(owner, "ready")
	update_msg()


func interact_with() -> void:
	# Put anything exclusive to dialog interactables here
	update_msg()
	.interact_with()


func update_msg() -> void:
	msg = [character_name, char_state, my_npc]
