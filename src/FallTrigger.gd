extends Area


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var reset_point: NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "on_body_entered")


func on_body_entered(body: Node):
	if body is KinematicBody:
		body.global_transform.origin = \
			(get_node(reset_point) as Spatial).global_transform.origin
		if body.has_method("reset_go_to"):
			body.reset_go_to()
