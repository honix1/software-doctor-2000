class_name DialogBox
extends Control

export var dialog_file_path := "res://Dialog.json"
export var portrait_folder_path := "res://images/dialog/characters/"
export var text_speed := 0.05

var dialog_file := {}
var dialog := []

var is_world_object := false
var object_name := ""
var object_description := ""

var character := ""
var char_state := ""
var dialog_index := 0
var finished := false

var player_portrait = preload("res://images/dialog/characters/doctor-neutral.png")
var narrator_portrait # could be blank
# Other portraits should be in the format "name_state_portrait.png"

onready var speaker_name: RichTextLabel = $MarginContainer/VBoxContainer/MarginContainer/Name
onready var dialog_text: RichTextLabel = $MarginContainer/VBoxContainer/MarginContainer2/Dialog
onready var portrait: Sprite = $Portrait
onready var timer: Timer = $Timer
onready var next_arrow: Polygon2D = $ArrowContainer/NextArrow


func _ready() -> void:
	timer.wait_time = text_speed
	if not is_world_object:
		dialog_file = load_dialog_file()
		dialog = load_dialog()
	else:
		dialog = load_popup()
	next_phrase()


func init_dialog(_character: String, _char_state: String) -> void:
	character = _character
	char_state = _char_state


func init_popup(_object_name: String, _object_desc: String) -> void:
	is_world_object = true
	object_name = _object_name
	object_description = _object_desc


func load_popup() -> Array:
	return [object_name, object_description]
	


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("next_dialog"):
		if finished:
			next_phrase()
		else:
			dialog_text.visible_characters = len(dialog_text.text)
		get_tree().set_input_as_handled()


func _process(delta: float) -> void:
	next_arrow.visible = finished


func load_dialog_file() -> Dictionary:
	var f := File.new()
	assert(f.file_exists(dialog_file_path), "Dialog file does not exist.")
	
	f.open(dialog_file_path, File.READ)
	var json := f.get_as_text()
	
	var output = parse_json(json)
	
	if typeof(output) == TYPE_DICTIONARY:
		return output
	else:
		return {}


func load_dialog() -> Array:
	var dialog := []
	
	if dialog_file.has(character) and dialog_file[character].has(char_state):
		dialog = dialog_file[character][char_state]
	
	return dialog


func next_phrase() -> void:
	if dialog_index >= len(dialog):
		queue_free()
		return
	
	finished = false
	
	if not is_world_object:
		Talkiewalkie.talk()
		speaker_name.bbcode_text = dialog[dialog_index]["Speaker"]
		dialog_text.bbcode_text = dialog[dialog_index]["Text"]
		set_portrait(dialog[dialog_index]["Speaker"])
	else:
		speaker_name.bbcode_text = object_name
		dialog_text.bbcode_text = object_description
		set_portrait("")
		dialog_index = 1 # hack
	
	dialog_text.visible_characters = 0
	
	while dialog_text.visible_characters < len(dialog_text.text):
		dialog_text.visible_characters += 1
		timer.start()
		yield(timer, "timeout")
	
	finished = true
	dialog_index += 1
	return


func set_portrait(speaker: String) -> void:
	var portrait_texture
	if speaker == "Player":
		portrait_texture = player_portrait
	elif speaker == "Narrator":
		portrait_texture = narrator_portrait
	elif speaker == "":
		portrait.hide()
		return
	else:
		var file_name = character + ".png" 
		portrait_texture = load(portrait_folder_path + file_name)
	
	portrait.texture = portrait_texture
