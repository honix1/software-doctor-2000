extends Spatial

onready var npcs := $NPCs.get_children()

func _ready() -> void:
	for npc in npcs:
		npc.player = $Player
		npc.nav = $Navigation
