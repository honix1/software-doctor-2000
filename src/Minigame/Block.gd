class_name Block
extends Area2D

export(bool) var is_frozen := false
export(bool) var is_moving_right := true

var speed := 30.0
#var _collider_margin := 0.0

onready var rect_shape: RectangleShape2D = $CollisionShape2D.shape
onready var nine_patch: NinePatchRect = $NinePatchRect


func _physics_process(delta: float) -> void:
	if is_frozen:
		return
	
	position.x += speed * delta * (1 if is_moving_right else -1)


func resize(width: float) -> void:
	nine_patch.rect_size.x = width
	nine_patch.rect_position.x = -width / 2
	rect_shape.extents.x = (width / 2)
