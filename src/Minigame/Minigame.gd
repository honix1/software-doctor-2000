class_name Minigame
extends Node2D

signal victory

export(PackedScene) var block_row_scene

export var row_height := 80

onready var player: PlayerMG = $PlayerMG
onready var player_start: Position2D = $Rows/PlayerStart


func _ready() -> void:
	player.connect("player_hit", self, "_on_player_hit")
	player.y_move_dist = row_height
	player.lower_bound_y = player_start.position.y
	player.upper_bound_y = $Goal.position.y
	$AnimationPlayer.play("player_setup")
	$AnimationPlayer.play("player_enter")


func _create_block_row(
	brick_width: float,
	brick_gap: float,
	brick_speed: float,
	alternates := false
) -> BlockRow:
	var row: BlockRow
	
	return row


func _reset_player() -> void:
	player.position = player_start.position


func _win() -> void:
	emit_signal("victory")
	print("win")
	#victory animation?
	#queue_free() #this happens on Main


func _on_player_hit() -> void:
	_reset_player()


func _on_Goal_area_entered(area: Area2D) -> void:
	_win()
