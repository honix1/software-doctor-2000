class_name BlockRow
extends Node2D

# Use spawn_delay_time to stagger the block spawn time (useful if another row has 
# the exact same speed, width, and gap settings)
export(bool) var has_spawn_delay := false
export(float) var spawn_delay_time := 0.0
export(float) var speed := 300.0
export(float) var block_width := 128.0
export(float) var gap_between_blocks := 512.0
export(bool) var move_right := true
export(PackedScene) var block_scene

var is_spawning := true

onready var direction := (1 if move_right else -1)
var blocks := []

onready var left_spawn: float = $BlockSpawnLeft.position.x
onready var right_spawn: float = $BlockSpawnRight.position.x
onready var spawn_delay_timer: Timer = $SpawnDelayTimer


func _ready() -> void:
	if has_spawn_delay:
		is_spawning = false
		spawn_delay_timer.start(spawn_delay_time)
		yield(spawn_delay_timer, "timeout")
		is_spawning = true


func _process(delta: float) -> void:
	if is_spawning:
		_handle_block_spawning(gap_between_blocks, block_width)


func _handle_block_spawning(
	gap_between := 512.0,
	width := 128.0
) -> void:
	
	var spawn_pos := left_spawn if move_right else right_spawn
	var last_block: Block
	
	if blocks.empty():
		last_block = _spawn_block(spawn_pos, width)
	else:
		var i := blocks.size() - 1
		last_block = blocks[i]
	
	var last_block_distance_traveled := abs(last_block.position.x - spawn_pos)
	if last_block_distance_traveled >= gap_between:
		_spawn_block(spawn_pos, width)


func _spawn_block(x_pos: float, width := 128.0) -> Block:
	var block: Block = block_scene.instance()
	add_child(block)
	blocks.append(block)
	
	block.position.x = x_pos
	block.resize(width)
	block.speed = speed
	block.is_moving_right = move_right
	
	return block


func _on_BlockKillerLeft_area_entered(area: Area2D) -> void:
	area.queue_free()
	blocks.erase(area)


func _on_BlockKillerRight_area_entered(area: Area2D) -> void:
	area.queue_free()
	blocks.erase(area)
