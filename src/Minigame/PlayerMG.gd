class_name PlayerMG
extends Area2D

signal player_hit

export(float) var h_speed := 200.0
export(float) var echo_input_delay := 0.1

var y_move_dist := 80.0
var echo_timer := echo_input_delay

var lower_bound_y: float
var upper_bound_y: float


##### Vertical movement (discrete) #####
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("move_forward", true):
		echo_move(event, "up")
	if event.is_action_pressed("move_backward", true):
		echo_move(event, "down")


# Player can hold down Up/Down to keep moving in that direction
# Echo timer decrements in _process
func echo_move(event: InputEvent, dir: String) -> void:
	if dir != "up" and dir != "down":
		printerr("Check player unhandled input code for typo.")
		return

	if not event.is_echo() or echo_timer <= 0.0:
		call("move_" + dir)
		echo_timer = echo_input_delay


func move_up() -> void:
	position.y -= y_move_dist # Could get each Layer's y_position instead
	position.y = clamp(position.y, upper_bound_y, lower_bound_y)


func move_down() -> void:
	position.y += y_move_dist
	position.y = clamp(position.y, upper_bound_y, lower_bound_y)
########################################


##### Horizontal movement (analog) #####
func _process(delta: float) -> void:
	var h_direction := get_horizontal_input()
	move_left_and_right(h_direction, delta)
	
	if echo_timer >= 0.0:
		echo_timer -= delta


func move_left_and_right(h_direction, delta) -> void:
	position.x += h_direction * h_speed * delta


func get_horizontal_input() -> float:
	var h_input := 0.0
	h_input = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	return h_input
########################################


func _on_PlayerMG_area_entered(area: Area2D) -> void:
	if area is Block:
		emit_signal("player_hit")

