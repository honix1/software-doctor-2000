extends MeshInstance

signal activate
signal deactivate(body)

export var prev_button_path: NodePath
var prev_button

onready var active_material: Material = preload("res://materials/wire_active.material")
onready var inactive_material: Material = preload("res://materials/wire_inactive.material")

onready var area: Area = $FloorButton/Area
var wire: MeshInstance

var initial_pos: Vector3
var active = false

func _ready():
	area.connect("body_entered", self, "body_entered")
	area.connect("body_exited", self, "body_exited")
	
	wire = find_node("Wire_*")
	
	initial_pos = global_transform.origin
	
	prev_button = get_node_or_null(prev_button_path)
	if prev_button:
		prev_button.connect("activate", self, "on_prev_activate")
		prev_button.connect("deactivate", self, "on_prev_deactivate")

func on_prev_activate():
	for b in area.get_overlapping_bodies():
		if b is KinematicBody:
			body_entered(b)
			break

func on_prev_deactivate(body):
	body_exited(body)

func body_entered(body: Node):
	if not active and (prev_button == null or prev_button.active):
		if body is KinematicBody:
			active = true
			
			Talkiewalkie.click()
			
			wire.material_override = active_material
		
			global_transform.origin = initial_pos + Vector3.DOWN * 0.1
			
			yield(get_tree().create_timer(2), "timeout")
			
			if active:
				emit_signal("activate")
#	else:
#		yield(prev_button, "activate")
#		if body in area.get_overlapping_bodies():
#			body_entered(body)

func body_exited(body: Node):
	if body is KinematicBody:
		active = false
		wire.material_override = inactive_material
		
		global_transform.origin = initial_pos
		
		emit_signal("deactivate", body)
