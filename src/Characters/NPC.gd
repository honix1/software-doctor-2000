class_name NPC
extends KinematicBody

export(String) var character_name
enum CharacterStates { BROKEN, FIXED }
export(CharacterStates) var char_state := 0 setget set_char_state
export(PackedScene) var my_minigame

enum States { IDLE, STOP_AND_FACE_PLAYER, GO_TO }
var state: int = States.IDLE setget _set_state

# Can be used to keep the character from wandering too far away
var origin_point := Vector3.ZERO
var nav: Navigation
var path := []
var path_node := 0
var player: Player
var go_to_target := Vector3.ZERO

onready var character_mover: CharacterMover = $CharacterMover
onready var model: Spatial = $Model
var interactable: InteractableDialog

func _set_state(_state: int):
	state = _state
	set_animation_by_state(_state)

func set_animation_by_state(_state: int):
	var a = find_node("AnimationPlayer")
	if _state == States.GO_TO:
		a.play("Run", 0.5)
	else:
		a.play("Idle", 0.5)

func _ready() -> void:
	character_mover.init(self, model)
	origin_point = global_transform.origin
	
	set_animation_by_state(state)
	
	interactable = get_node_or_null("Model/InteractableDialog")
	
	if interactable:
		interactable.icon.visible = false
		interactable.character_name = character_name
		self.char_state = char_state # setter will update Interactable with correct String
		interactable.my_npc = self


func _physics_process(delta: float) -> void:
	match state:
		States.IDLE:
			process_state_idle(delta)
		States.GO_TO:
			process_state_go_to(delta)
		States.STOP_AND_FACE_PLAYER:
			process_state_stop_and_face_player(delta)


func process_state_idle(delta: float) -> void:
	character_mover.move_vector = Vector3.ZERO
	
	if player is Player:
		var player_near = (transform.origin - player.transform.origin).length() < 5.0
		if player_near:
			self.state = States.STOP_AND_FACE_PLAYER


func process_state_stop_and_face_player(delta: float) -> void:
	character_mover.move_vector = Vector3.ZERO
	character_mover.face_move_direction = false
	
	var look_target = player.global_transform.origin
	character_mover.smooth_look_at(look_target, delta)
	
	if (transform.origin - player.transform.origin).length() >= 6.0:
		self.state = States.IDLE


func process_state_go_to(delta: float) -> void:
	character_mover.face_move_direction = true
	if path_node < path.size():
		var direction = (path[path_node] - global_transform.origin)
		if direction.length() < 1:
			path_node += 1
		else:
			direction = direction.normalized()
			var almost_there = global_transform.origin.distance_to(go_to_target) < 3
			if almost_there:
				# drop the moving speed
				direction *= 0.25
			character_mover.direct_set_move_vector(direction)
	else:
	#if global_transform.origin.distance_to(go_to_target) < 0.1:
		self.state = States.IDLE
		# Clean up (I know this should go into an exit_state function but oh well) 
		path = []
		path_node = 0
		go_to_target = Vector3.ZERO
		interactable.set_active()
	
	#Debug print
#	if path_node == path.size() - 1:
#		prints("my pos", global_transform.origin)
#		prints("target", path[path_node])


# The Main script can use this to make the NPC move to a location
func go_to(target: Vector3) -> void:
	interactable.set_inactive()
	go_to_target = target
	self.state = States.GO_TO
	path = nav.get_simple_path(global_transform.origin, go_to_target)

func reset_go_to():
	go_to(go_to_target)

# This is for dialog/story, not move/behavior states
# Sorry for the confusing naming >_>
func set_char_state(value: int) -> void:
	value = clamp(value, 0, CharacterStates.size() - 1)
	char_state = value
	
	if interactable:
		var _state_string = "Broken" if char_state == CharacterStates.BROKEN else "Fixed"
		interactable.char_state = _state_string


func get_interactable() -> Interactable:
	return interactable

func salut():
	$WinCPUParticles.emitting = true

#func process_state_wander(delta: float) -> void:
#	character_mover.is_frozen = false
#	character_mover.face_move_direction = true
#	character_mover.move_vector = Vector3.FORWARD # TEMP/DEBUG
#
#	if player:
#		var player_near = (transform.origin - player.transform.origin).length() < 10.0
#		if player_near:
#			state = States.STOP_AND_FACE_PLAYER


# Work in progress
#func get_random_wander_point(origin: Vector3, max_distance_from: float) -> Vector3:
#	randomize()
#	var goal := origin
#
#	goal.x = origin.x + rand_range(0.0, max_distance_from)
#	goal.y = origin.y + rand_range(0.0, max_distance_from)
#
#	return goal
