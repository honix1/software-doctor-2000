class_name Player
extends KinematicBody

export(bool) var invert_camera_x := false
export(bool) var invert_camera_y := false
export(float, 0.1, 1.0) var mouse_sensitivity: float = 0.3
export(float, -90.0, 0.0) var min_pitch: float = -90
export(float,  0.0, 90.0) var max_pitch: float =  90
export var turn_speed := 10.0

# Camera Pivot will rotate around Y axis, Spring will rotate around X axis
onready var camera_pivot: Spatial           = $CameraPivot
onready var camera_spring: SpringArm        = $CameraPivot/SpringArm
onready var camera: Camera                  = $CameraPivot/SpringArm/Camera
onready var character_mover: CharacterMover = $CharacterMover
onready var model: Spatial                  = $Doctor
onready var animation_tree: AnimationTree   = $Doctor/AnimationTree
onready var interact_raycast: RayCast       = $Doctor/InteractRayCast

var smooth_velocity: Vector3
var last_collider: Interactable

func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	character_mover.init(self, model)
	# The Interactable node on NPCs and objects is an Area
	interact_raycast.collide_with_areas = true
	interact_raycast.collide_with_bodies = false
	
	if not check_is_on_floor():
		animation_tree_jump(true)


func _unhandled_input(event: InputEvent) -> void:
	# DEBUG
	if event.is_action_pressed("debug_quit"):
		get_tree().quit()
	# DEBUG
	if event.is_action_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		OS.window_fullscreen = false
	
	if event is InputEventMouseButton:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	if event is InputEventMouseMotion:
		camera_pivot.rotation_degrees.y -= event.relative.x * mouse_sensitivity * (1 if invert_camera_x else -1)
		camera_spring.rotation_degrees.x -= event.relative.y * mouse_sensitivity * (1 if invert_camera_y else -1)
		camera_spring.rotation_degrees.x = clamp(camera_spring.rotation_degrees.x, min_pitch, max_pitch)
	
	if character_mover.is_frozen:
		return
	
	if event.is_action_pressed("jump"):
		character_mover.jump()
		$IsOnFloorTimer.start(0.03) # see "check_is_on_floor()" comment for why this exists
		animation_tree_jump(true)


func _physics_process(delta: float) -> void:
	if character_mover.is_frozen:
		return
	
	character_mover.move_vector = get_input_direction()
	
	var collider = interact_raycast.get_collider()
	# Update the last collider the player was near when he moves away
	if last_collider and not collider  == last_collider:
		last_collider.is_player_near = false
	
	if collider is Interactable:
		last_collider = collider
		collider.is_player_near = true
		if Input.is_action_just_pressed("interact"):
			collider.interact_with()


##### TODO: Change this to a raycast so it works more consistently with abnormal floor meshes
# When Jump is pressed, a very short IsOnFloorTimer starts to make sure we don't get
# is_on_floor() == true on the frame immediately after we jump
func check_is_on_floor() -> bool:
	var check := true
	
	if $IsOnFloorTimer.time_left == 0:
		check = is_on_floor()
	else:
		check = false
	
	return check


func get_input_direction() -> Vector3:
	var dir := Vector3.ZERO
	var forward = camera_pivot.global_transform.basis.z.normalized()
	
	if Input.is_action_pressed("move_forward"):
		dir -= forward
	if Input.is_action_pressed("move_backward"):
		dir += forward
	if Input.is_action_pressed("move_left"):
		dir += forward.cross(Vector3.UP)
	if Input.is_action_pressed("move_right"):
		dir -= forward.cross(Vector3.UP)
	
	return dir.normalized()


func _process(delta):
	animation_tree_update(delta)


func animation_tree_update(delta):
	if check_is_on_floor():
		animation_tree_jump(false)
	
	smooth_velocity = smooth_velocity.linear_interpolate(character_mover.velocity, 25 * delta)
	
	animation_tree.set("parameters/RunBlend/blend_amount", 
		smooth_velocity.length() / character_mover.max_speed)


func animation_tree_jump(active = true):
	var state := int(active) # active == 1, not active == 0
	animation_tree.set("parameters/AirTransition/current", state)


func freeze() -> void:
	character_mover.is_frozen = true


func unfreeze() -> void:
	character_mover.is_frozen = false
