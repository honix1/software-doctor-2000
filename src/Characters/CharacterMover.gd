class_name CharacterMover
extends Spatial

signal movement_info

var body_to_move: KinematicBody = null
var model: Spatial = null

export var face_move_direction := true

export var max_speed: float = 10.0
export var acceleration: float = 5.0
export var air_acceleration: float = 5.0
export var gravity: float = 0.98
export var max_fall_speed: float = 54.0
export var jump_impulse: float = 20.0
export var turn_speed: float = 10.0

var move_vector := Vector3.FORWARD setget _set_move_vector
var velocity := Vector3.ZERO
var pressed_jump := false
var y_velocity := 0.0

var is_frozen := false


func init(_body_to_move: KinematicBody, _character_model: Spatial):
	body_to_move = _body_to_move
	model = _character_model

func direct_set_move_vector(_move_vec: Vector3):
	# this set didn't use setter
	move_vector = _move_vec

func _set_move_vector(_move_vec: Vector3):
	move_vector = _move_vec.normalized()


func _physics_process(delta: float) -> void:
	handle_movement(delta)


func handle_movement(delta: float) -> void:
	if is_frozen:
		velocity = Vector3.ZERO
		return
	
	var is_on_floor: bool = body_to_move.is_on_floor()
	
	var accel := acceleration if is_on_floor else air_acceleration
	velocity = velocity.linear_interpolate(move_vector * max_speed, accel * delta)
	
	if is_on_floor:
		y_velocity = -0.01
	else:
		y_velocity = clamp(y_velocity - gravity, -max_fall_speed, max_fall_speed)
	
	if pressed_jump and is_on_floor:
		y_velocity = jump_impulse
	
	pressed_jump = false
	
	var floor_max_angle := deg2rad(50)
	
	velocity.y = y_velocity
	body_to_move.move_and_slide(velocity, Vector3.UP, 
		false, 4, floor_max_angle)
	
	if face_move_direction and move_vector.length() > 0.1:
		var look_target := move_vector + global_transform.origin
		smooth_look_at(look_target, delta)
	
	emit_signal("movement_info", velocity, is_on_floor)


func jump() -> void:
	pressed_jump = true


# TODO - Make this only rotate around Y axis:
#        I think it's fun that the NPCs bend backward to see the player jump, but 
#        it probably won't look good when they have real models
func smooth_look_at(look_target: Vector3, delta: float) -> void:
	var t: Transform = model.global_transform
	var lerp_target := t.looking_at(look_target, Vector3.UP)
	
	# looking_at method consider -Z is forward, but our models is Z forwarded
	# this code does rotation to make looking_at Z forwaded
	var basis: Basis = lerp_target.basis
	basis = basis.rotated(basis.y, PI)
	lerp_target = Transform(basis, lerp_target.origin)
	
	var new_t := model.global_transform.interpolate_with(lerp_target, turn_speed * delta)
	model.global_transform = new_t
