extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var best_quality := true setget _set_best_quality

# Called when the node enters the scene tree for the first time.
func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event is InputEventKey and event.is_pressed():
		if event.scancode == KEY_F:
			OS.window_fullscreen = !OS.window_fullscreen
		elif event.scancode == KEY_L:
			self.best_quality = !self.best_quality


func _set_best_quality(b: bool):
	best_quality = b
	
	var gips := get_tree().root.find_node("GIProbeSmall", true, false) as GIProbe
	var gipb := get_tree().root.find_node("GIProbeBig", true, false) as GIProbe
	var e := ($WorldEnvironment as WorldEnvironment).environment
	
	if best_quality:
		gips.visible = true
		gipb.visible = true
		
		e.background_energy = 1
		e.tonemap_mode = Environment.TONE_MAPPER_ACES
		e.auto_exposure_enabled = true
		e.ssao_enabled = true
		e.glow_enabled = true
		
		ProjectSettings.set("rendering/quality/shadows/filter_mode", 1)
		ProjectSettings.set("rendering/quality/filters/msaa", 4)
	else:
		gips.visible = false
		gipb.visible = false
		
		e.background_energy = 16
		e.tonemap_mode = Environment.TONE_MAPPER_LINEAR
		e.auto_exposure_enabled = false
		e.ssao_enabled = false
		e.glow_enabled = false
		
		ProjectSettings.set("rendering/quality/shadows/filter_mode", 0)
		ProjectSettings.set("rendering/quality/filters/msaa", 0)
