extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var axis := Vector3(0.1, 0.1, 0.1).normalized()
var taken := false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$coin.rotate(axis, delta)


func _on_Area_body_entered(body):
	if not taken and body is Player:
		taken = true
		Talkiewalkie.coin()
		var t := 0.0
		while t < 1:
			global_transform.origin = \
				global_transform.origin.linear_interpolate(body.global_transform.origin + Vector3.UP, t)
			scale = Vector3.ONE * (1 - t)
			t += get_tree().root.get_process_delta_time()
			yield(get_tree(), "idle_frame")
		queue_free()
