extends Spatial

export(Array, NodePath) var interactable_objects
var _interactables := []
export(Array, NodePath) var character_nodes
var _characters := []

var is_dialog_open := false
var npc_talking_to: NPC
var fixed_npcs := 0 # used to send each character to the correct main puzzle button

var dialog_box_scene := preload("res://src/UI/DialogBox.tscn")

onready var player: Player = $Characters/Player
onready var nav: Navigation = $Navigation
# Main puzzle button locations
onready var top_button_pos: Vector3 = $MainPuzzleButtonPositions/TopButton.transform.origin
onready var bot_button_pos: Vector3 = $MainPuzzleButtonPositions/BottomButton.transform.origin
onready var left_button_pos: Vector3 = $MainPuzzleButtonPositions/LeftButton.transform.origin
onready var right_button_pos: Vector3 = $MainPuzzleButtonPositions/RightButton.transform.origin


func _ready() -> void:
	setup_characters()
	connect_interactables()


func setup_characters() -> void:
	for node_path in character_nodes:
		var character = get_node(node_path)
		if character is NPC:
			_characters.append(character)
			character.player = player
			character.nav = nav


func connect_interactables() -> void:
	for obj in interactable_objects:
		var interactable = get_node(obj).get_interactable()
		_interactables.append(interactable)
		var output = interactable.get_interactable().connect("interacted_with", self, "_on_Interactable_interacted_with")


func new_dialog(character: String, char_state: String) -> void:
	if is_dialog_open:
		return
	
	is_dialog_open = true
	var dialog_box = dialog_box_scene.instance()
	dialog_box.init_dialog(character, char_state)
	dialog_box.connect("tree_exited", self, "_on_dialog_finished")
	$CanvasLayer/GUI.add_child(dialog_box)
	player.freeze()


func new_popup(object_name: String, text_to_show: String) -> void:
	if is_dialog_open:
		return
	
	is_dialog_open = true
	var dialog_box = dialog_box_scene.instance()
	dialog_box.init_popup(object_name, text_to_show)
	dialog_box.connect("tree_exited", self, "_on_dialog_finished")
	$CanvasLayer/GUI.add_child(dialog_box)
	player.freeze()


func start_minigame(minigame: PackedScene) -> void:
	var mg: Minigame = minigame.instance()
	add_child(mg)
	player.freeze()
	yield(mg, "victory")
	Talkiewalkie.win()
	player.unfreeze()
	npc_talking_to.char_state = NPC.CharacterStates.FIXED
	npc_talking_to.salut()
	mg.queue_free()
	new_dialog(npc_talking_to.character_name, npc_talking_to.interactable.char_state)


# I want to make sure the first NPC we fix goes to the furthest button
# to avoid NPCs pushing each other off the buttons
func send_npc_to_main_puzzle(npc: NPC) -> void:
	match fixed_npcs:
		0:
			npc.go_to(top_button_pos)
		1:
			npc.go_to(right_button_pos)
		2:
			npc.go_to(left_button_pos)
	
	fixed_npcs += 1


func _on_dialog_finished() -> void:
	player.unfreeze()
	
	if npc_talking_to:
		if npc_talking_to.char_state == NPC.CharacterStates.BROKEN:
			var minigame = npc_talking_to.my_minigame
			start_minigame(minigame)
		elif npc_talking_to.char_state == NPC.CharacterStates.FIXED:
			send_npc_to_main_puzzle(npc_talking_to)
			npc_talking_to = null
	
	# quick timer to make sure we don't immediately open another dialog
	# it's hacky but whatever
	yield(get_tree().create_timer(0.5), "timeout")
	is_dialog_open = false


# Minigames might get triggered from dialog? I'm not sure what Bakenshake is doing yet
func _on_Interactable_interacted_with(InteractionType: int, msg: Array) -> void:
	match InteractionType:
		Interactable.InteractionTypes.DIALOG:
			# msg: [char_name, char_state]
			var char_name: String = msg[0]
			var char_state: String = msg[1]
			var npc: NPC = msg[2]
			npc_talking_to = npc
			new_dialog(char_name, char_state)
		Interactable.InteractionTypes.WORLD_OBJECT:
			# msg: [world_object.name, text_to_display]
			var object_name = msg[0]
			var text_to_display = msg[1]
			new_popup(object_name, text_to_display)
#		Interactable.InteractionTypes.MINIGAME:
#			# msg: [which_character_minigame]
#			var char_name: String = msg[0]
#			var npc: NPC = msg[1]
#			start_minigame(char_name, npc)
